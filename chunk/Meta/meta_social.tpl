<meta property="og:locale" content="ru_RU"/>[[
<meta property="og:type" content="[+type:ne=``:then=`article`+]"/> ]]
<meta property="og:title" content="[*longtitle:ifempty=`[*pagetitle*]`*]"/>
[*description:notempty=`<meta property="og:description" content="[*description*]"/>`*]
[*img:notempty=`<meta property="og:image" content="[(site_url)][*img*]"/>`*]
<meta property="og:url" content="[(site_url)][+phx:input=`[+url+]`:ne=``:then=`[+url+]`:else=`[*id:ne=`1`:then=`[~[*id*]~]`*]`+]" />
<meta property="og:site_name" content="[+phx:input=`[+site_name+]`:ne=``:then=`[+site_name+]`:else=`[(site_name)]`+]"/>