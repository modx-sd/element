	</div>
	<div class="footer">
		<div class="mainw">
			<div class="menu">{{FooterMenu}}</div>
			<div class="wrap">
				<div class="section">[[ShowBlock? &name=`tplFooterAdd` &id=`1` &out=`%s`]]</div>
				<div class="copy"><a href="/" class="logo" title="[(site_name)]"></a>
					@[[currentYear]] <a href="/">{{domain}}</a>
				</div>
				<div class="section">

					<!-- noindex -->
					<div class="counters">{{counters}}</div>
					<!-- /noindex -->
					<div class="webmaster">{{WebmasterLogo}}</div>
				</div>
			 </div>
		</div>
	</div>
</body>
</html>
